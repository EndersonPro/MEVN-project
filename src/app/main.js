import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import VueAxios from "vue-axios";
import axios from "axios";
Vue.use(VueAxios, axios);

import App from "./App.vue";

import DisplayItem from "./components/DisplayItem.vue";
import CreateItem from "./components/CreateItem.vue";


const routes = [
  {
    name: "DisplayItem",
    path: "/",
    component: DisplayItem
  },
  {
    name: "CreateItem",
    path: "/add/item",
    component: CreateItem
  }
];

const router = new VueRouter({ mode: "history", routes: routes });
new Vue(Vue.util.extend({ router }, App)).$mount("#app");
