const moongoose = require('mongoose');
const Schema = moongoose.Schema;

const Item = new Schema({
    name: { type: String },
    price: { type: Number }
},{
    collection: 'items'
});

module.exports = moongoose.model('Item', Item);