const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors')

const itemRoutes = require('./routes/item');

mongoose.connect('mongodb://localhost:27017/mevn-stack', { useNewUrlParser: true })
    .then(()=> console.log('Base de datos conectada'))
    .catch( err => console.log(err));

//Setting
app.set('port', process.env.PORT || 3000);

//routes - REST API
app.use('/item', itemRoutes);

//Middlewres
app.use(cors());
app.use(bodyParser.json())

//Static Files
app.use(express.static(path.join(__dirname, 'public')));


app.listen(app.get('port'), ()=>{
    console.log('Servidor en puerto ', app.get('port'))
})