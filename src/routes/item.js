const express = require('express');
const router = express.Router();

const Item = require('../models/item');



//Get Data
router.get('/', (req, res) => {

    Item.find((err, items) => {

        if (err) { throw err; }
        else {
            res.json(items)
        }

    })

})

//Add Data
router.post('/', (req, res) => {

    const item = new Item(req.body);

    item.save().then(item => {
        res.status(200).json({
            item: 'Item Agregado'
        })
    }).catch(err => {
        res.status(400).send({
            item: ' Error al agregar item '
        })
    })

})

router.put('/:id', (req, res, next) => {
    Item.findById(req.params.id, (err, item) => {
        if (!item) {
            return next(new Error('no se pudo cargar el documento'))
        } else {
            item.name = req.body.name;
            item.price = req.body.price;
            item.save().then(item => {
                res.json('Actualizacion completa')
            }).catch(err => {
                res.status(400).send('No se pudo actualizar')
            });
        }
    })
})


router.delete('/:id', (req, res, next) => {
    Item.findByIdAndRemove(req.params.id, (err, item) => {
        if (err) { res.json(err); }
        else {
            res.json('Item eliminado correctamente')
        }
    })
})



module.exports = router;